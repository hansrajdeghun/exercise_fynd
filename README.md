# Exercise_fynd_academy_python_on_boarding 

This repository is the solution to the exercises from Fynd Academy Python Track Engineering On-Boarding.
https://gofynd.quip.com/ph76ALBxji1V/Fynd-Academy-Python-Track-Engg-On-Boarding-copy

## Git Exercise


* Create a repo on Gitlab
* Set it up on your system
* Set up these git aliases (https://www.freecodecamp.org/news/how-to-use-git-aliases/) to improve your git productivity (Alternative tutorial here (https://snyk.io/blog/10-git-aliases-for-faster-and-productive-git-workflow/))


## GitLab Exercise

* Create and add your SSH public key (https://docs.gitlab.com/ee/ssh/index.html), for enabling Git over SSH.
* Create a merge request (https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html), to request changes made in a branch be merged into a project’s repository.
* Setup 2FA (https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html) on your Gitlab Account

## Code Quality Exercises


* Install flake8 (https://github.com/pycqa/flake8), configure it and run it on your past projects
    * Post the sample output of flake8
    * Correct the violations mentioned by flake8 and re-post the output

* Based on your learning in code smells and clean-code-python, share a list of the code smells in your past projects
    * Use the refactoring (https://refactoring.guru/refactoring/techniques) suggestions and refactoring features in your IDE to refactor your code
    * Share a PR with refactored code for review

## Advanced Python Exercise


* Find out the time complexity of all operations on a list and dictionary in python
* Complete this quiz on python dicts (https://realpython.com/quizzes/python-dicts/) and post a screenshot of your results
* Complete this quiz on reading and writing files in Python (https://realpython.com/quizzes/read-write-files-python/)
* Complete all problems in python-workshop (https://gitlab.com/fynd/ops-engg/miscellaneous/python-workshop) and raise a Pull Request
    * https://anandology.com/python-practice-book/object_oriented_programming.html (Extend python-workshop with this) Pending

## Asyncio and Concurrency Exercise

Complete this Python Concurrency (https://realpython.com/quizzes/python-concurrency/) Quiz and post the screenshot of your results

## Documentation

Documentation of the same is present inside documentation folder
