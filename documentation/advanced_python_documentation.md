Data Structure: provides the way of arranging the data so it can be accessed efficiently.
	• Dictionary: key value pair, O(1) for insert, delete, upload average case. Keys can only  be immutable data type only hash able.
		○ collections.OrderedDict: Remember the Insertion Order of Keys
		○ collections.ChainMap: Search Multiple Dictionaries as a Single
		○ Mappingtypes.MappingProxyType: A Wrapper for Making Read-Only Dictionaries
	• Array: An array is a collection of items stored at contiguous memory locations.list:
		○ Mutable Dynamic Arrays
		○ tuple: Immutable Containers
		○ array.array: Basic Typed Arrays
		○ str: Immutable Arrays of Unicode Characters
		○ bytes objects are immutable sequences of single bytes (0-255)
		○ bytearray: Mutable Arrays of Single Bytes(0-255)
		○ Records, Structs, and Data Transfer Objects:
		Compared to arrays, record data structures provide a fixed number of fields.
	•  Classes: allow you to define reusable blueprints for data objects to ensure each object provides the same set of fields.
		○ dataclasses.dataclass: They provide an excellent alternative to defining your own data storage classes from scratch.
			§ The syntax for defining instance variables is shorter, since you don’t need to implement the .__init__() method.
			§ Instances of your data class automatically get nice-looking string representation via an auto-generated .__repr__() method.
			§ Instance variables accept type annotations, making your data class self-documenting to a degree. Keep in mind that type annotations are just hints that are not enforced without a separate type-checking tool.
	• Sets. Frozensets
	• collections.Counter: Multisets
		○ The collections.Counter class in the Python standard library implements a multiset, or bag, type that allows elements in the set to have more than one occurrence.
		○ This is useful if you need to keep track of not only if an element is part of a set, but also how many times it’s included in the set:
		
		>>> from collections import Counter
		>>> inventory = Counter()
		
		>>> loot = {"sword": 1, "bread": 3}
		>>> inventory.update(loot)
		>>> inventory
		Counter({'bread': 3, 'sword': 1})
		
		>>> more_loot = {"sword": 1, "apple": 1}
		>>> inventory.update(more_loot)
		>>> inventory
		Counter({'bread': 3, 'sword': 2, 'apple': 1})
	• Stacks (LIFOs): A stack is a collection of objects that supports fast Last-In/First-Out (LIFO) semantics for inserts and deletes.The insert and delete operations are also often called push and pop.
	• collections.deque: Fast and Robust Stacks
		○ The deque class implements a double-ended queue that supports adding and removing elements from either end in O(1) time (non-amortized). Because deques support adding and removing elements from either end equally well, they can serve both as queues and as stacks.
	• Queues (FIFOs):
		○ A queue is a collection of objects that supports fast FIFO semantics for inserts and deletes. The insert and delete operations are sometimes called enqueue and dequeue. 
	• Assignment vs. Shallow Copy vs. Deep Copy in Python
		○ Assignment Operator (=):  it doesn’t  make a copy of python object instead, it copies a memory address
		○ Shallow Copy: A shallow copy constructs a new compound object and then (to the extent possible) inserts references into it to the objects found in the original.
		
		nums = [1, 2, 3, 4, 5]      
		>>> import copy
		>>> m1 = copy.copy(nums)  # make a shallow copy by using copy module
		>>> m2 = list(nums)       # make a shallow copy by using the factory function
		>>> m3 = nums[:]          # make a shallow copy by using the slice operator
		
		○ Deep Copy: A deep copy constructs a new compound object and then, recursively, inserts copies into it of the objects found in the original.
		import copy
		>>> a = [[1, 2, 3], [4, 5, 6]]  
		>>> b = copy.deepcopy(a)                     
		>>> id(a) == id(b)
		False
		>>> id(a[0]) == id(b[0])      # memory address is different      
		False
		>>> a[0].append(8)            # modify the list                                                                                                                                            
		>>> a
		[[1, 2, 3, 8], [4, 5, 6]]
		>>> b
		[[1, 2, 3], [4, 5, 6]]        # New list's elements didn't get affected

	• Concurrency: The dictionary definition of concurrency is simultaneous occurrence
	
	Concurrency Type						Switching Decision															Number of Processors
	Pre-emptive multitasking (threading)	The operating system decides when to switch tasks external to Python.		1
	Cooperative multitasking (asyncio)		The tasks decide when to give up control.									1
	Multiprocessing (multiprocessing)		The processes all run at the same time on different processors.				Many
	
	
	• Thread: A thread is a separate flow of execution.
	Because of the way CPython implementation of Python works, threading may not speed up all tasks. This is due to interactions with the GIL that essentially limit one Python thread to run at a time.
	
	• Daemon Threads : In computer science, a daemon is a process that runs in the background.
	
	
	Python threading has a more specific meaning for daemon. A daemon thread will shut down immediately when the program exits. One way to think about these definitions is to consider the daemon thread a thread that runs in the background without worrying about shutting it down.
	
	• Coroutiens: https://stackabuse.com/coroutines-in-python/
	• Every programmer is acquainted with functions - sequences of instructions grouped together as a single unit in order to perform predetermined tasks. They admit a single entry point, are capable of accepting arguments, may or may not have a return value, and can be called at any moment during a program's execution - including by other functions and themselves.
	
	• Coroutines are a special type of function that deliberately yield control over to the caller, but does not end its context in the process, instead maintaining it in an idle state.
	• They benefit from the ability to keep their data throughout their lifetime and, unlike functions, can have several entry points for suspending and resuming execution.
	Coroutines in Python work in a very similar way to Generators.
	Both operate over data, so let's keep the main differences simple
		○ Generators produce data
		○ Coroutines consume data
	
				def bare_bones():
				    print("My first Coroutine!")
				    while True:
				        value = (yield)
				        print(value)
				coroutine = bare_bones()
				
	If this were a normal Python function, one would expect it to produce some sort of output by this point. But if you run the code in its current state you will notice that not a single print() gets called.
	
	That is because coroutines require the next() method to be called first:
		def bare_bones():
		    print("My first Coroutine!")
		    while True:
		        value = (yield)
		        print(value)
		
		coroutine = bare_bones()
		next(coroutine)
	
	
	
