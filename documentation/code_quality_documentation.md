Python PEP8:

Aim to enhance readability
	• Indentation(prefer space over tab)
	• Maximum Line Length(72/79)
	• Break after binary operators
	• 2 blank line before new class/function 
	• Imports should usually be on separate lines
	• Class names starts with upper case
	• Functions starts with lower case
	• Constants should have all upper case character

Code Smells:
	• Bloaters
		○ Bloaters Bloaters are code, methods and classes that have increased to such gargantuan proportions that they are hard to work with.
			§ Long Method : should not be more than 10-15 lines (treatment: split)
			§ Large Class: A class contains many fields/methods/lines of code. (treatment: split)
			§ Primitive Obsession : Just a field for storing some data!( treatment: Replace Data Value with Object.)
			§ Long Parameter List : (p>4)
			§ Data Clumps: Sometimes different parts of the code contain identical groups of variables (such as                    parameters for connecting to a database). These clumps should be turned into their own classes.


Clean Code:
	• Variables:
		○ Use meaningful and pronounceable variable names
		○ Use the same vocabulary for the same type of variable
		○ Avoid Mental Mapping : sq in sqs location in locations
		○ Avoid unnecessary adding context 
		○ Use default arguments instead of short circuiting or conditionals
		○ Functions should do one thing
		○ Function names should say what they do
		○ Functions should only be one level of abstraction
		○ Don't use flags as function parameters
		○ Avoid side effects
	• Class:
		○ Single Responsibility Principle (SRP) 
		○ Open/Closed Principle (OCP) Software entities should be open for extension, but closed for modification.
		○ Liskov Substitution Principle (LSP)
		○ Interface Segregation Principle (ISP) Clients should not be forced to depend upon interfaces that they dont use“.
		○ Dependency Inversion Principle (DIP)
	• Don't repeat yourself (DRY)
