Git
 -> free open source version control system
Version control-> management of changes to documents, computer programs, large websites, and other collections of information.

Git Commands
	• Clone -> Bring a repository that is hosted somewhere like Github into a folder on your local machine
	• Add -> Track your files and changes in Git (git add filename)
	• Commit -> Save your files in Git (git commit -m "added/removed something" -m "description"
	• Push -> Upload Git commits to a remote repo like Github/Gitlab (git push)
	• Pull -> Download changes from remote repo  to your local machine, opposite of push
	• Git status -> shows all files that are updated added removed but havent been savefdf in commit yet also shows tracked and untracked files.
	• Git init -> initializes empty git repository
	• SSH keys : inorder to push into github/gitlab we have to prove that we are the owner of that account , we can use ssh command (ssh-keygen -t rsa -b 4096 -C "email.com")
		○ -t rsa specifies  type of algorithm 
		○ -b 4096  defines strength of algoritm 
		○ -c 'email.com' specifies our email
	• With SSH keys, if someone gains access to your computer, they also gain access to every system that uses that key. To add an extra layer of security, you can add a passphrase to your SSH key. You can use ssh-agent to securely save your passphrase so you don't have to re-enter it.
	• Add SSH pub key in Github
	• Run SSH agent (SSH-agent) 
	• eval "$(ssh-agent -s)"
	• Add SSH private key to our agent (ssh-add ~/testkey)
	• Git remote add: To add a new remote, use the git remote add command on the terminal, in the directory your repository is stored at.
		○ the git remote add command takes two arguments: A remote name, for example, origin 
		○ A remote URL, for example, https://github.com/user/repo.git
	• Git remote -v : shows all the connected remote to this remote
	• Git push origin master: suppose if I don’t want to this whole command we use git push -u origin master to make this default remote and  we don’t have to specify next time.
	• Git workflow -> code -> stage changes(git add) -> commit changes (git commit) -> push changes (git push) -> make pull request (git pull)
	• Git branching: git branch command to see current branch
	• Git checkout: create or change branch:
		○ Git checkout -b feature-name : to create new branch name feature-name
		○ Git checkout master: to switch into master branch
	• Git diff name-of-branch: what changes have been made 
	• Git merge:  to merge two branch
	• Git branch -d feature-branchname : to delete a branch
	• Git reset (unstage):  undo 
	• Reset to before commit: git reset HEAD~1 means go before 1 commit
	• Git log : gives log of commits
	• Go to specific commit : git reset HASH-that -we-got-from-log
	• Git reset --hard (not just upstaged but completely remove)
	• Git fork : A fork is a copy of a repository. Forking a repository allows you to freely experiment with changes without affecting the original project.
Best Practices in Git ->
	• Make Atomic git commits its benefits:
		○  Atomic commits make it easier to track down regressions
		○ Atomic commits are easier to revert, drop, and amend
		○ Atomic commits make it easier to complete larger tasks
	• Write Good Commit messages:
		○ Capitalized, short (50 chars or less) summary
		○ More detailed explanatory text, if necessary.  Wrap it to about 72
		characters or so.
		○ Specify the type of commit:
			§ feat: The new feature you're adding to a particular application
			§ fix: A bug fix
			§ style: Feature and updates related to styling
			§ refactor: Refactoring a specific section of the codebase
			§ test: Everything related to testing
			§ docs: Everything related to documentation
			§ chore: Regular code maintenance.[ You can also use emojis to represent commit types]
Advanced Git ->
	• Interactive Rebase (rewrites commits)
		○ Change a commits message
		○ Delete commits
		○ Reorder commits
		○ Combine multiple commits into one
		○ Edit/split an existing commit into multiple new ones
		○ Note do not use interactive rebase on commits that we already pushed on remote repository
		○ Examples:
			§ Reword ( change commit message)
				□ Git log --oneline
				□ Git rebase -i HEAD~3 ( if we want to change most recent commit use commit --ammed)
				□ Editor window will be opened replace pick by reword to modify the commit message save and close another window will open there add the new commit message
				□ Git log --oneline ( to check )
			§ Squash ( combine 2 commits into one )
				□ Git log --oneline
				□ Git rebase -I HEAD~4 
				□ Editor will get opened replace pick with squash it combines that and the one above commit
				□ Save and close another window will get opened add message save and close
				□ Git log --oneline
	• Cherry picking 
	
		○ Git cherry-pick hash-of-commit
		○ Git reset --hard HEAD~1
		
	• Reflog: (Git diary/ journal logs every movement of head pointer)
		Reflog for deleted commits
		Git reset --hard commithash
		Git reflog get the hash where we want to undo
		Git branch name hash-that-we-got-from-reflog
		We can also restore branches
		Git branch name hash

	